import pytest
import base.constants as const
from selenium.webdriver import Chrome


@pytest.fixture(scope="session")
def browser():
    """
    Devuelve una instancia del navegador Chrome
    :return: instancia
    """
    driver = Chrome()
    driver.implicitly_wait(const.IMPLECIT_WAIT)
    driver.maximize_window()

    yield driver
    driver.quit()
