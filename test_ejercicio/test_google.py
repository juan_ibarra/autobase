from base.pages.homepage_google import HomePageGoogle
from base.locator import Locator
from selenium.webdriver.common.by import By
from base.base import BaseElement


class TestGoogle:

    def test_busqueda(self, browser):
        home = HomePageGoogle(browser)
        home.go()

        # home.search_field.input_text("Hola")
        # home.search_button.click_element()
        home.search_something("uzbekistan")

    def test_busqueda2(self, browser):
        home = HomePageGoogle(browser)
        home.go()
        home.search_something("Georgia")

        txt = Locator(by=By.ID, value="result-stats")
        base = BaseElement(browser, txt)
        print(f"+++++++++++++++++ Texto encontrado: {base.get_text} ++++++++++++++++++++++")
        home.validate_result()
