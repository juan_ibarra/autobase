import constants as const
from typing import Tuple
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BaseElement:

    def __init__(self, driver, locator: Tuple):
        """

        :param driver: webdriver
        :param locator: tuple of type Locator
        """
        self.driver = driver
        self.locator = locator

        self.web_element = None
        self.find()

    def open_browser(self, url: str):
        self.driver.get(url)
        self.driver.maximize_window()

    def find(self):
        element = WebDriverWait(self.driver, const.IMPLECIT_WAIT).until(
            EC.visibility_of_element_located(locator=self.locator)
        )
        self.web_element = element
        return None

    def input_text(self, text):
        self.web_element.send_keys(text)
        return None

    def click_element(self):
        element = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locator=self.locator)
        )
        element.click()
        return None

    def get_attribute(self, attr_name):
        attribute = self.web_element.get_attribute(attr_name)
        return attribute

    @property
    def get_text(self):
        text = self.web_element.text
        return text

    # TODO: agregar mas keywords
    # wait until element is visible
    # page should contain
    # page should contain element
    # click button
    # click link
    # clear element text
    # close browser
