from selenium.webdriver.common.by import By
from base.base import BaseElement
from base.base_page import BasePage
from base.locator import Locator


class HomePageGoogle(BasePage):
    url = "https://www.google.com"

    @property
    def search_field(self):
        element = Locator(by=By.NAME, value="q")
        return BaseElement(self.driver, element)

    @property
    def search_button(self):
        element = Locator(by=By.NAME, value="btnK")
        return BaseElement(self.driver, element)

    def search_something(self, text):
        self.search_field.input_text(text)
        self.search_button.click_element()

    @property
    def sarasa(self):
        element = Locator(by=By.NAME, value="qqw")
        try:
            return BaseElement(self.driver, element)
        except:
            print(f"ERROR: elemento <{element.by}: {element.value}> no encontrado")

    def validate_result(self):
        self.sarasa.find()

    @property
    def texto(self):
        element = Locator(by=By.ID, value="result-stats")
        return BaseElement(self.driver, element)

