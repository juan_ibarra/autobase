class BasePage:

    url = None

    def __init__(self, driver):
        self.driver = driver

    def go(self):
        # BaseElement.open_browser(self.url)
        self.driver.get(self.url)
        self.driver.maximize_window()
